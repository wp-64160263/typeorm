import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"
AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepository = AppDataSource.getRepository(Product)
    

    console.log("Loading product from the database...")
    const product = await productRepository.find()
    console.log("Loaded product: ", product)


    const updatedProduct = await productRepository.findOneBy({id:1})
    console.log(updatedProduct);
    updatedProduct.price = 80;
    await productRepository.save(updatedProduct)
    

}).catch(error => console.log(error))
